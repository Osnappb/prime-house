# Prime House Productions #



### [Perona Farms Wedding](https://www.primehouseproductions.com/photo-booth/tracy-james-perona-farms-wedding/) ###

* located Andover, NJ
* Wedding venue
* outdoor park to capture beautiful photographs
* 200 occupancy 

### [legacy castle wedding](https://www.primehouseproductions.com/film/ravi-mansi-legacy-castle-wedding-film/) ###

* located prompton plains, nj
* castle
* occupancy 300
* beautiful lake outside with plety of open space to capyure photographs
* architecture building
* American & Indian Wedding

### [merigold somerset nj](https://www.primehouseproductions.com/indian-sweet-sixteen/aloki-sweet-sixteen-marigold-somerset-nj/) ###

* located somerset, nj - South Jersey
* mutltiple ballrooms
* occupancy 300-400 
* plenty of space to capture photographs
* weddings and non-wedding events 


### [Sweet 16](https://www.primehouseproductions.com/indian-sweet-sixteen/vritti-sweet-16-theme-party/) ###

* located Ford, NJ - Central Jersey
* multiple ballrooms
* occupancy up to 600 
* can handle up to 4-5 events at the same time
* outdoor fountain 
* outdoor cocktail session
* weddings and non-wedding events 

visit [Indian Wedding Photographer](https://www.primehouseproductions.com) for more details